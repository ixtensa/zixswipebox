
# README #

!!! IMPORTANT NOTICE !!!

* The parts of files are changed for the Contao optimization
* For original files or more details visit: http://brutaldesign.github.io/swipebox/

* Version: Extension include scripts only for Contao <3.2

### What is this repository for? ###

* Gallery with swipe gestures for tablets and smartphones

### How do I get set up? ###
1. Copy folder to */system/modules/*.
2. Choose in page layout jQuery-Templates *j_swipebox*.
2.1. If you check *j_swipebox* you musst remove the standad *j_colorbox* from your layout


### How it's work? ###

The files */system/modules/assets/js/jQuery.swipebox.js* or */system/modules/assets/js/jQuery.swipebox.min.js* included automaticaly if device are a Mobile, Android or IOS

Template */system/modules/templates/j_swipebox.html5* change all images with a-tags and *data-lightbox* attributes. See code below
All images with *a-tag* in *.ce_gallery*, *.ce_image* or *.image_container_* will open in fullsize and you can use the swipe gestrues to slide images left->right or right->left.

	(function($) {
		$('.ce_gallery a,.ce_text a[data-lightbox],.ce_image a[data-lightbox],.image_container a[data-lightbox],a[data-lightbox]').swipebox({
			useCSS : true, // false will force the use of jQuery for animations
			useSVG : true, // false to force the use of png for buttons
			initialIndexOnArray : 0, // which image index to init when a array is passed
			hideCloseButtonOnMobile : false, // true will hide the close button on mobile devices
			hideBarsDelay : 3000, // // delay in ms(3000) before hiding bars on desktop, set value on false to show allways
			videoMaxWidth : 600, // videos max width
			beforeOpen: function() {}, // called before opening
			afterOpen: null, // called after opening
			afterClose: function() {}, // called after closing
			loopAtEnd: false // true will return to the first image after the last image is reached
		});
	})(jQuery);

### Who do I talk to? ###

* Hakan Havutcuoglu
* info@havutcuoglu.com