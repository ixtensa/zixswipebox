<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package   SwipeBox
 * @author    Hakan Havutcuoglu
 * @license   GNU
 * @copyright IXTENSA
 */

if (TL_MODE == 'FE') {
	
	//JS
	$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/zixSwipebox/assets/js/jquery.swipebox.min.js|static';
	
	//CSS
	$GLOBALS['TL_CSS'][] = 'system/modules/zixSwipebox/assets/css/swipebox.min.css|static';

}