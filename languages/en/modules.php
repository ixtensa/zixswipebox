<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   zixSwipebox
 * @author    Hakan Havutcuoglu
 * @license   GNU
 * @copyright IXTENSA 2015
 */


/**
 * Back end modules
 */

$GLOBALS['TL_LANG']['MOD']['zixSwipebox'][0] = 'IXTENSA Smartphone Gallery';